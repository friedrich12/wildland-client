# HTTP plugin

HTTP storage backend for Wildland client.

### Getting started

```bash
wl storage create http --container MYCONTAINER --url HTTPURL
```
