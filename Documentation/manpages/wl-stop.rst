.. program:: wl-stop
.. _wl-stop:

:command:`wl stop` --- Stop the Wildland FUSE driver
====================================================================

Synopsis
--------

:command:`wl stop`

Options
-------

No options.
